import React from "react";
import Navbar from "@/components/Navbar/Navbar";
import { Box} from "@material-ui/core";
import Footer from "@/components/Footer/Footer"

function Layout({children}: any) {
  return (
    <Box style={{ position: "relative" }}>
        <Navbar/>
        <main>{children}</main>
        <Footer/>
    </Box>
    
  )
}

export default Layout