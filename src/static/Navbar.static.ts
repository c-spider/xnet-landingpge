import DiscordSVG from "@/public/assets/svgs/discord.svg"
import TwitterSVG from "@/public/assets/svgs/twitter.svg"
import YoutubeSVG from "@/public/assets/svgs/youtube.svg"
import { INavbar } from "@/types"


export const NavbarLinks: INavbar[] = [
    {
        label: 'Home',
        link:"/",
        color: "#fff",
        cursor: "pointer",
    },
    {
        label: 'Documents',
        link:"/documents",
        color: "#fff",
        cursor: "pointer",
    },
    {
        label: '$XNET Token',
        link:"/xnet-token",
        color: "#fff",
        cursor: "pointer",
    },
    {
        label: 'Shop',
        link:"/shop",
        color: "#fff",
        cursor: "pointer",
    },
    {
        label: 'Cart',
        link:"/",
        color: "#4A4A4A",
        cursor: "not-allowed",
    },
    {
        label: 'Account',
        link:"/",
        color: "#4A4A4A",
        cursor: "not-allowed",
    },
]

export const NavbarSocialLinks = [
    {
        SVG: DiscordSVG,
        link: "https://discord.gg/3W5vTU8aCn",
    },
    {
        SVG: TwitterSVG,
        link: "https://twitter.com/XNET_Mobile",
    },
    {
        SVG: YoutubeSVG,
        link: "https://www.youtube.com/channel/UCyIptNxGEgzXpckV5EL38qQ",
    },
]