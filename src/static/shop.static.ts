import Item01 from "@/public/assets/images/item01.png";
import Item02 from "@/public/assets/images/item02.png";

export const products = [
  {
    id: "1",
    productImage: Item01,
    title: "XI1 Felix",
    stars: 2,
    reviews: "3 review",
    price: "1,795.00",
  },
  {
    id: "2",
    productImage: Item02,
    title: "XO2 Lucius",
    stars: 5,
    reviews: "5 review",
    price: "2,195.00",
  },
];
