import { IDifferenceSection } from "@/types";
import Icon1 from "@/public/assets/svgs/icon1.svg";
import Icon2 from "@/public/assets/svgs/icon2.svg";
import Icon3 from "@/public/assets/svgs/icon3.svg";
import Icon4 from "@/public/assets/svgs/icon4.svg";

export const DiffernceStaticData: IDifferenceSection[] = [
    {
        heading: "Problem: Current State of Mobile",
        content: "Millions of people in the United States are still underconnected. Cellular plans are still expensive, while networks struggle with capacity issues and coverage gaps.",
        id: 1,
        Icon: Icon1
    },
    {
        heading: "Solution: XNET ",
        content: "XNET's fast, reliable and secure cellular service will be deployed by a dedicated community of node operators. Node operators will receive crypto mining rewards and free mobile service in exchance for filling  in coverage gaps and expanding existing network capacity.",
        id: 2,
        Icon: Icon2
    },
    {
        heading: "XNET Service",
        content: "XNET is a full-featured mobile network that offers carrier-grade secure access to phone numbers, voice, SMS messaging and emergency services (e911).",
        id: 3,
        Icon: Icon3
    },
    {
        heading: "XNET Blockchain",
        content: "Node operator validation is completed on-chain with operator mining rewards paid out in $XNET. The XNET Foundation utilizes a DAO governance model to balance the needs and interests of all stakeholders.",
        id: 4,
        Icon: Icon4
    },
]