import Paper from "@/public/assets/documentsIcon/Vector.png";
import Decentralized from "@/public/assets/svgs/decentralized.svg";
import Sustainablity from "@/public/assets/svgs/sustainablity.svg";

export const generalDocuments: any = [
  {
    id: 1,
    title: "Whitepaper",
    subTitle: "Evething you need to know about XNET",
    IconUrl: Paper,
    href: "/files/Whitepaper.pdf",
  },
  {
    id: 2,
    title: "Decentralization",
    subTitle: "XNET and governance",
    IconUrl: Decentralized,
    href: "/files/Governance.pdf",
  },
  {
    id: 3,
    title: "Sustainability",
    subTitle: "XNET and Sustainability",
    IconUrl: Sustainablity,
    href: "/files/Sustainability.pdf",
  },
];

export const hardwareDataSheets: any = [
  {
    id: 1,
    title: "XI1 Felix",
    IconUrl: Paper,
    href: "/",
  },
  {
    id: 2,
    title: "XO1 Lucius",
    IconUrl: Paper,
    href: "/",
  },
];
