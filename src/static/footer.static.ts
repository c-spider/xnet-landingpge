import Discord from "@/public/assets/svgs/discord.svg";
import Twitter from "@/public/assets/svgs/twitter.svg";
import Youtube from "@/public/assets/svgs/youtube.svg";

export const footerMenu = [
  {
    id: 1,
    title: "Privacy Policy",
    href: "/privacy-policy",
  },
  {
    id: 2,
    title: "Terms Of Use",
    href: "/",
  },
];

export const footerSvg = [
  {
    id: 1,
    SVG: Discord,
    src: "https://discord.gg/3W5vTU8aCn",
  },
  {
    id: 2,
    SVG: Twitter,
    src: "https://twitter.com/XNET_Mobile",
  },
  {
    id: 3,
    SVG: Youtube,
    src: "https://www.youtube.com/channel/UCyIptNxGEgzXpckV5EL38qQ",
  },
];
