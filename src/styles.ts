import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
    BoxContainer: {
        [theme.breakpoints.down('sm')]: {
            marginTop: '0 !important',
        },
    },

}));
