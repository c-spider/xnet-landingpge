import { Box, Typography } from '@material-ui/core'
import Image from 'next/image'
import React from 'react'
import BackButton from '../Reusable-components/Back-Button/BackButton'
import useStyles from 'src/components/XnetTokens/XnetTokens.styles'
import { data } from './XnetTokensData'
import { data1 } from './XnetTokensData'

const XnetTokensContent = () => {
    const classes = useStyles();
    return (
        <Box className={classes.mainContainer}>
            <BackButton/>
            <Typography className={classes.mainHeading}>$XNET Token</Typography>
            <Typography className={classes.subHeading}>XNET Mobile Network</Typography>
            <Typography className={classes.innerContent}>
                Unlike traditional wireless networks that are dependent on wireless spectrum licenses and lots of
                expensive infrastructure (like RAN/cell site assets), XNET will own no spectrum and very little physical
                network infrastructure. These network assets will be acquired, deployed and operated by a dedicated
                community of individual XNET enablers and professional network deployment companies and ISPs. Everyone
                involved in the deployment and operation of the XNET network will be rewarded by receiving $XNET crypto
                tokens in return for various activities that they undertake.
            </Typography>
            <Typography className={classes.subHeading}>$XNET Tokenomics and Allocation</Typography>
            <Typography className={classes.innerContent}>
                There will be a total max supply of <Typography className={classes.innerContentColored}
                                                                component="span">24,000,000,000</Typography> (twenty-four
                billion) deflationary XNET
                tokens
                ($XNET). These tokens will be issued gradually over time and be allocated according to the breakdown
                below.
            </Typography>
            <Image src={'/assets/token_img/pieChart.png'}  alt={'Chart photo'} width={1032} height={500}/>
            <Box className={classes.serviceWrapper}>
                {data.map(item =>
                    <Box className={classes.singleService} key={item.id}>
                        <Image src={item.img} className={classes.singleImg} alt={item.alt}/>
                        <Typography className={classes.serviceContent}>{item.title}</Typography>
                    </Box>
                )}
            </Box>
            <Box className={classes.serviceWrapperSecond}>
                {data1.map(item =>
                    <Box className={classes.singleService} key={item.id}>
                        <Image src={item.img} className={classes.singleImg} alt={item.alt}/>
                        <Typography className={classes.serviceContent}>{item.title}</Typography>
                    </Box>
                )}
            </Box>
            <Typography className={classes.subHeading}>
                Location and Clustering
            </Typography>
            <Typography className={classes.innerContent}>
                XNET intends to strategically place a limited number of its long-range and short-range nodes in a given
                area, thus balancing coverage size and quality of service with mining rewards. This strategic approach
                to network deployment results in an overall higher network value for mobile operating partners, superior
                network end-user experience and consistently attractive rewards for the community of XNET operators.
            </Typography>
            <Typography className={classes.innerContent}>
                When a consumer purchases one or more nodes from XNET, they indicate in which location they will bring
                the node into operation. This is validated at first on our website against our list of ‘allowed’
                locations. This is to ensure that we are building clusters of nodes to create a viable mobile phone
                network.
            </Typography>
            <Typography className={classes.innerContent}>
                Once the operator registers their new node on the network, we check again that the location is as
                expected. If the node is operated in a location outside of tolerance from that expected, no token
                rewards are generated.The location is checked on registration and continually by the validation
                algorithms on the network.
            </Typography>
            <Typography className={classes.subHeading}>
                Token Rewards
            </Typography>
            <Typography className={classes.innerContent}>
                The detailed explanation of the tokenomics model behind the $XNET token can be found in the XNET
                whitepaper.
            </Typography>
            <Typography className={classes.innerContent}>
                There are 4 ways that tokens can be earned by these operators:
            </Typography>
            <ol>
                <li className={classes.boldInnerContent}>Registration</li>
                <Typography className={classes.innerContent}>
                    When a consumer or professional purchases one or more nodes from XNET, they indicate in which
                    location they will bring the node into operation. For consumers, this is validated at first on our
                    website against our allowed locations (as above).Once the registration process is passed
                    successfully, some tokens are released to the operator based on their type of node (see below). This
                    is a one-time grant of tokens, with a lockup period of one year.
                </Typography>
                <li className={classes.boldInnerContent}>Coverage</li>
                <Typography className={classes.innerContent}>
                    This will be a regular disbursement of tokens to an operator in reward for them having an
                    operational node on the network. These tokens will be proportionate to the type of node and its
                    coverage capabilities.
                </Typography>
                <li className={classes.boldInnerContent}>Validation</li>
                <Typography className={classes.innerContent}>
                    This will be a regular release of tokens to an operator in reward for them participating in a
                    validation exercise. It is certain that we will reward the maximum such tokens to our node operators
                    early on in the network, as long as they have an operational node.
                </Typography>
                <li className={classes.boldInnerContent}> Data throughput</li>
                <Typography className={classes.innerContent}>
                    Eventually, this will be the heart of how both operators and XNET earn rewards. Based on measured
                    data throughput (in GB) through the node, XNET will grant a number of tokens in proportion to that
                    throughput. Data throughput is expected within the first 12 months of inital network deployment.
                </Typography>
            </ol>
            <Typography className={classes.subHeading}>Staking</Typography>

            <Typography className={classes.innerContent}>
                The XNET Rewards Function determines the reward in XNET granted to a nominally-functioning node or
                validator at the end of a staking epoch. This function applies both to full XNET nodes and validators,
                though validators do not receive a coverage score or data score.
            </Typography>
            <Typography className={classes.boldInnerContent} style={{maxWidth: '600px'}}>
                Repoch = DeploymentBonus4 +
                min(stake/10, ZoneMultiplier * DemandMultiplier * QualityMultiplier *
                (coverage_score + data_score + validation_score)) - <br/>
                XNETMNOfee
            </Typography>
            <Typography className={classes.innerContent}>
                Except for a one-time deployment bonus, the maximum reward available in an epoch is capped to 1/10th the
                amount staked, as assurance of good behavior and quality. Stakes are subject to slashing if quality is
                consistently low or cheating is detected. The node or validator operator may adjust the stake for the
                next epoch up or down at any time, but the stake used for this calculation is the stake at the beginning
                of an epoch, which is locked for the duration.
            </Typography>
            <Typography className={classes.subHeading}>Staking</Typography>
            <Typography className={classes.cursiveText}>*For the first 12 months of network deployment.</Typography>
            <table className={classes.tableWrapper}>
                <tr>
                    <th className={classes.tableHeaderItem}>Categories</th>
                    <th className={classes.tableHeaderItem}>XI1</th>
                    <th className={classes.tableHeaderItem}>XO1</th>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Product Name</td>
                    <td className={classes.tableItem}>Felix</td>
                    <td className={classes.tableItem}>Lucius</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>SKU</td>
                    <td className={classes.tableItem}>ISU-430IS</td>
                    <td className={classes.tableItem}>OSU-430IM</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Components</td>
                    <td className={classes.tableItem}>1xXI1 Felix 430 antenna, 1xXNET
                        Core server
                    </td>
                    <td className={classes.tableItem}>1xXO1 Lucius 430i antennas, 1xXNET
                        Core server
                    </td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Product Price</td>
                    <td className={classes.tableItem}>$1,795</td>
                    <td className={classes.tableItem}>$2,195</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Launch Registration tokens</td>
                    <td className={classes.tableItem}>10,000</td>
                    <td className={classes.tableItem}>10,000</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Launch Coverage tokens (per month)</td>
                    <td className={classes.tableItem}>250</td>
                    <td className={classes.tableItem}>250</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Launch Validation tokens (per month)</td>
                    <td className={classes.tableItem}>250</td>
                    <td className={classes.tableItem}>250</td>
                </tr>
                <tr>
                    <td className={classes.highliteTable}>Launch Data tokens (per month)</td>
                    <td className={classes.tableItem}>-</td>
                    <td className={classes.tableItem}>-</td>
                </tr>
            </table>
        </Box>
    )
}

export default XnetTokensContent
