import { makeStyles } from '@material-ui/core';

export default makeStyles({
    mainContainer: {
        paddingLeft: '8vw',
        paddingRight: '8vw',
        paddingTop: '4rem',
        paddingBottom: '4rem',
        backgroundColor: '#0E0E11'
    },
    mainHeading: {
        fontFamily: '\'Orbitron\'',
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '60px',
        lineHeight: '75px',
        background:
            'linear-gradient(221.21deg, #75FFF6 23.34%, #37BBF8 42.85%, #0685F9 77.24%)',
        WebkitBackgroundClip: 'text',
        WebkitTextFillColor: 'transparent',
        backgroundClip: 'text',
        textFillColor: 'transparent',
        marginBottom: '4rem'
    },
    subHeading: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '32px',
        lineHeight: '38px',
        marginBottom: '1rem'
    },
    innerContent: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 400,
        fontSize: '20px',
        lineHeight: '28px',
        marginBottom: '3rem',
        color: 'background: rgba(255, 255, 255, 1)',
        '@media(max-width: 430px)': {
            fontSize: '16px',
            marginBottom: '1rem',
        }
    },
    innerContentColored: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 400,
        fontSize: '20px',
        lineHeight: '28px',
        marginBottom: '3rem',
        color: '#00F8F1',
        '@media(max-width: 430px)': {
            fontSize: '16px',
            marginBottom: '1rem',
        }
    },
    ulContainer: {
        marginBottom: '3rem'
    },
    innerContentUL: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 400,
        fontSize: '20px',
        lineHeight: '146.02%',
        marginBottom: '1rem',
        color: 'background: rgba(255, 255, 255, 1)'
    },
    boldInnerContent: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '20px',
        lineHeight: '146.02%',
        marginTop: '2rem',
        marginBottom: '3rem'
    },
    serviceWrapper: {
        display: 'grid',
        gridTemplateColumns: '.8fr 1.4fr .8fr',
        gridGap: '20px',
        alignItems: 'center',
        marginTop: '50px',
        '@media(max-width: 1329px)': {
            gridTemplateColumns: '1fr 1fr 1fr',
        },
        '@media(max-width: 1061px)': {
            gridTemplateColumns: '1fr 1fr',
        },
        '@media(max-width: 720px)': {
            gridTemplateColumns: '1fr',
        },
    },
    serviceWrapperSecond: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr',
        gridGap: '20px',
        // alignItems: "center",
        marginTop: '20px',
        '@media(max-width: 1061px)': {
            gridTemplateColumns: '1fr 1fr',
        },
        '@media(max-width: 720px)': {
            gridTemplateColumns: '1fr',
        },
        marginBottom: '50px',
    },
    singleService: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '2rem',
        background: ' #16181D',
        borderRadius: '20px',
        height: '350px !important',
        justifyContent: 'center',
        '@media(max-width: 1329px)': {
            padding: '1rem',
            height: '350px !important',
        },
        '@media(max-width: 430px)': {
            height: '400px !important',
        }
    },
    singleImg: {
        maxWidth: '366px',
        maxHeight: '48px',
        marginBottom: '84px',
        objectFit: 'contain',
    },
    serviceContent: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 400,
        fontSize: '16px',
        lineHeight: '146.02%',
        color: 'background: rgba(255, 255, 255, 1)',
        paddingTop: '48px',
        textAlign: 'center',
        maxWidth: '430px',
        maxHeight: '145px'
    },
    tableWrapper: {
        maxWidth: '1230px',
        background: 'inherit',
        textAlign: 'start',
        borderCollapse: 'separate',
        borderSpacing: '.5em',
    },
    tableHeaderItem: {
        background: '#0685F9',
        padding: '10px',
        fontSize: '18px',
        lineHeight: '16px',
        fontWeight: 400,
        borderRadius: '7px',
        maxWidth: '403px'
    },
    cursiveText: {
        fontStyle: 'italic',
        fontWeight: 400,
        fontSize: '18px',
        lineHeight: '21.09px',
        marginBottom: '30px'
    },
    highliteTable: {
        background: '#16181D',
        padding: '10px',
        margin: '5px 5px 5px 0',
        color: '#0685F9',
        fontSize: '16px',
        lineHeight: '16px',
        borderRadius: '7px',
        '@media(max-width: 430px)': {
            fontSize: '12px',
        }
    },
    tableItem: {
        background: '#16181D',
        padding: '10px',
        margin: '5px 5px 5px 0',
        color: '#fff',
        fontSize: '16px',
        lineHeight: '16px',
        borderRadius: '7px',
        '@media(max-width: 430px)': {
            fontSize: '12px',
        }
    },
    chartItem: {
         maxWidth: '366px',
        maxHeight: '48px',
        width: '100%',
        height: '100%',
        objectFit: 'contain',
    }
})
