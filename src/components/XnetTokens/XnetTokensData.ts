export const data = [
    {
        id:1,
        img: require("../../../public/assets/token_img/img1.png"),
        title: '$XNET is a true utility token, \n' +
            'converting to network data credits.',
        alt: 'information image'

    },
    {
        id:2,
        img: require("../../../public/assets/token_img/img2.png"),
        title: '$XNET is issued to operators for running XNET nodes, in proportion to \n' +
            '• data served to wireless clients,  \n' +
            '• value of the local market (urban, rural, international etc.) and \n' +
            '• network state / validation services.',
        alt: 'information image'
    },
    {
        id:3,
        img: require("../../../public/assets/token_img/img3.png"),
        title: '$XNET will be using proof of capacity / data served mining algorithm.',
        alt: 'information image'
    },
];
export const data1 = [
    {
        id:1,
        img: require("../../../public/assets/token_img/img4.png"),
        title: '• $XNET can be converted directly to data tickets and all data consumed on the network will be accounted for in this way. \n' +
            '• Converted tokens will be taken out of circulation (burned).',
        alt: 'information image'
    },
    {
        id:2,
        img: require("../../../public/assets/token_img/img5.png"),
        title: 'Individuals and organizations will pay for network services in $XNET.',
        alt: 'information image'
    },
    {
        id:3,
        img: require("../../../public/assets/token_img/img6.png"),
        title: 'Mobile Network Operators will pay XNET in USD or other FIAT currency in arrears for data consumed by their subscribers and these FIAT payments will be used to burn proportional numbers of $XNET, thus supporting their value.',
        alt: 'information image'
    },
];
