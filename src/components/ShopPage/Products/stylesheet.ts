import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  mainContainer: {
    display: "flex",
    flexDirection: "column",
    marginTop: "5rem",
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {
      marginTop: "10rem",
    },
  },

  productTypo: {
    fontFamily: "'Orbitron'",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "50px",
    lineHeight: "75px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "10vw",
    },
  },
}));
