import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "./stylesheet";
import Product from "../../Reusable-components/productBox/ProductBox";

function Products() {
  const classes = useStyles();
  return (
    <Box className={classes.mainContainer}>
      <Box>
        <Typography className={classes.productTypo}>Products</Typography>
      </Box>
      <Product />
    </Box>
  );
}

export default Products;