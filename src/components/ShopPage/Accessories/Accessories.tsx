import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "src/components/ShopPage/Accessories/Accessories.styles"

function Accessories() {
  const classes = useStyles()
  return (
    <Box>
      <Typography
      className={classes.accessories}
      >
        Accesories
      </Typography>
      <Typography
       className={classes.soon}
      >
        Cooming Soon...
      </Typography>
    </Box>
  );
}

export default Accessories;