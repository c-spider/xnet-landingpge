import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  accessories: {
    fontSize: "50px",
    fontWeight: 800,
    lineHeight: "100px",
    marginBottom: "5px",
    marginTop: "5rem",
    fontFamily: "Orbitron",
    [theme.breakpoints.down("sm")]: {
      fontSize: "35px",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "25px",
    },
  },
  soon: {
    fontFamily: "'Orbitron'",
    fontStyle: "normal",
    fontWeight: 900,
    fontSize: "32px",
    lineHeight: "40px",
    opacity: 0.4,
    [theme.breakpoints.down("sm")]: {
      fontSize: "19px",
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "18px",
      textAlign: "center"
    },
    '@media(max-width: 430px)': {
      marginTop: "-200px",
    },
  },
}));
