import { makeStyles } from '@material-ui/core';

export default makeStyles((theme) => ({
    newsMainContainer: {
        background: '#16181D',
        width: 'auto',
    },
    newsInnerContainer: {
        padding: '54px 9rem 1rem 9rem',
        width: '-webkit-fill-available',
        [theme.breakpoints.down('xs')]: {
            padding: '3rem 9rem 1rem 2rem',
        },
    },
    newMainHeadingTypo: {
        fontFamily: '\'Orbitron\'',
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '60px',
        lineHeight: '115px',
        background:
            'linear-gradient(221.21deg, #75FFF6 23.34%, #37BBF8 42.85%, #0685F9 77.24%)',
        WebkitBackgroundClip: 'text',
        WebkitTextFillColor: 'transparent',
        backgroundClip: 'text',
        textFillColor: 'transparent',
        [theme.breakpoints.down('xs')]: {
            fontSize: '24px',
            lineHeight: 'unset',
        },
    },
    comingSoonText: {
        fontFamily: '\'Orbitron\'',
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '48px',
        lineHeight: '60px',
        color: '#FFFFFF',
        opacity: '0.6',
        marginTop: '31px',
        [theme.breakpoints.down('xs')]: {
            fontSize: '18px',
            lineHeight: 'unset',
        },
    }
}));
