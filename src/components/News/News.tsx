import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "src/components/News/News.styles";
import useMediaQuery from "@mui/material/useMediaQuery";

const News = () => {
  const classes = useStyles();
  const matching = useMediaQuery("(max-width:550px)");

  return (
    <Box
      style={{ height: matching ? "14rem" : "346px" }}
      className={classes.newsMainContainer}
    >
      <Box className={classes.newsInnerContainer}>
        <Box>
          <Typography className={classes.newMainHeadingTypo}>{`News`}</Typography>
          <Typography className={classes.comingSoonText}>{`Coming soon`}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default News;
