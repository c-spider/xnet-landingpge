import { Box, Typography } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import React from 'react'
import useStyles from './stylesheet'
import { useRouter } from 'next/router';

const BackButton = () => {
    const router = useRouter()
    const classes = useStyles()

    // create a function to go back
    const goBack = () => {
        router.back()
    }

  return (
    <Box onClick={goBack} className={classes.mainButtonContainer}>
        <Box className={classes.iconContainer}>
            <ArrowBack style={{ fill: "#fff" }} />
        </Box>

        <Typography className={classes.typo}>Back</Typography>
    </Box>
  )
}

export default BackButton;
