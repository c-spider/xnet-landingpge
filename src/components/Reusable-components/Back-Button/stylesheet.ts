import { makeStyles } from "@material-ui/core";

export default makeStyles({
  mainButtonContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "0px",
    gap: "20px",
    marginBottom: "4rem",
    cursor: "pointer"
  },
  iconContainer: {},
  typo: {
    fontFamily: "'Roboto'",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "24px",
    lineHeight: "28px",
  },
});
