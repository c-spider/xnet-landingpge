import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "./stylesheet";
import Image from "next/image";
import Link from "next/link";
// import Rating from "@mui/material/Rating";
import { products } from "@/static/shop.static";

function ProductBox() {
  const classes = useStyles();

  return (
    <Box className={classes.productBox}>
      {products.map(({ id, productImage, title }) => {
        return (
            <Box key={id}>
              <Box className={classes.products}>
                <Box>
                  <Image
                    width={200}
                    height={200}
                    src={productImage}
                    alt="logo"
                  />
                </Box>
              </Box>
              <Box className={classes.productDetail}>
                <Box className={classes.productChild}>
                  <Typography>{title}</Typography>
                  <Box className={classes.reviewBox}>
                    {/* <Rating
                    size="small"
                    name="half-rating"
                    defaultValue={stars}
                    precision={0.5}
                    style={{ color: "#fff" }}
                  /> 
                  <Typography style={{ marginLeft: "2rem" }}>
                    {reviews}
                  </Typography> */}
                  </Box>
                  <Link className={classes.addToCart} href="/">
                    Add To Cart
                  </Link>
                </Box>
                <Box className={classes.productChild}>
                  {/* <Typography>{price}</Typography> */}
                </Box>
              </Box>
            </Box>
        );
      })}
    </Box>
  );
}

export default ProductBox;
