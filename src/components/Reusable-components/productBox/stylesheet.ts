import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  productBox: {
    display: "grid",
    gridTemplateColumns: "22% 22% 22% 22%",
    gridGap: "50px",
    marginTop: "2rem",
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "45% 45%",
    },
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "90%",
    },
  },
  products: {
    backgroundColor: "#16191E",
    borderRadius: "20px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: "2rem",
    paddingBottom: "2rem",
    height: "330px",
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {
      height: "400px",
    },
  },
  productDetail: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "1rem",
    padding: "5px",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
    [theme.breakpoints.down("xs")]: {
      flexDirection: "row",
    },
  },
  productChild: {
    display: "flex",
    flexDirection: "column",
  },
  addToCart: {
    marginTop: "4px",
    fontFamily: "'Roboto'",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "24px",
    lineHeight: "28px",
    textDecorationLine: "underline",
  },
  reviewBox: {
    display: "flex",
  },
}));
