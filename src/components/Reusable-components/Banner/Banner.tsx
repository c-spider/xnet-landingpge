import { Box, Button, Typography } from '@material-ui/core';
import React from 'react';
import Link from 'next/link';
import useStyles from 'src/components/Reusable-components/Banner/Banner.styles';

interface IProps {
    mainText: string;
    buttonText: string;
    fontSize?: string;
    link: string;
    mainTextMaxWidth?: number
    contentContainer?: number | string
}

const Banner = ({mainText, buttonText, fontSize = '32px', link, mainTextMaxWidth, contentContainer}: IProps) => {
    const classes = useStyles();
    return (
            <Box className={classes.bannerMainContainer}>
                <Box className={classes.contentContainer}>
                    <Box className={classes.textContainer} style={{width: contentContainer}}>
                    <Typography style={{fontSize, maxWidth: mainTextMaxWidth}} className={classes.contentTypo}>
                        {mainText}
                    </Typography>
                </Box>
                <Link href={link}>
                    <Button className={classes.button}>{buttonText}</Button>
                </Link>
                </Box>

            </Box>
    );
};

export default Banner;
