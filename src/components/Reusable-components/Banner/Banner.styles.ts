import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    bannerMainContainer: {
        width: '100%', backgroundColor: '#0685F9',
        [theme.breakpoints.down('sm')]: {
            marginTop: '0'
        },
    },
    contentContainer: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: '43px',
        paddingBottom: '42px',
        margin: 'auto',
        minHeight: '179px',
        gap: '35px',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
            margin: 'auto',
            alignItems: 'start',
            paddingTop: '37px',
            paddingBottom: '40px',
            marginLeft: '26px',
            marginRight: '15px',
        },
    },
    textContainer: {
        display: 'flex',
        alignItems: 'center',
        padding: '0px',
        maxWidth: '1019px',
        width: '100%',
        justifyContent: 'center',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'flex-start',
            width: '100%',
            textAlign: 'start',

        },

    },
    contentTypo: {
        fontFamily: '\'Roboto\'',
        fontStyle: 'normal',
        fontWeight: 500,
        fontSize: '32px',
        lineHeight: '146.02%',
        textAlign: 'end',
        [theme.breakpoints.down('sm')]: {
            textAlign: 'start'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'start',
            fontSize: '4vw !important'
        },
    },
    button: {
        boxSizing: 'border-box',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: '238px',
        minWidth: '164px',
        width: '100%',
        height: '60px',
        gap: '20px',
        color: '#fff',
        border: '2px solid #FFFFFF',
        borderRadius: '10px',
        textTransform: 'none',
        [theme.breakpoints.down('xs')]: {
            height: '45px'
        },
        '&:hover': {
            backgroundColor: 'black',
            border: '2px solid black',
        },
    },
}));