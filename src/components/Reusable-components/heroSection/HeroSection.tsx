import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "./stylesheet";
import Button from "@material-ui/core/Button";

const HeroSection = (props: any) => {
  const classes = useStyles();
  return (
    <Box className={classes.MainContainer}>
      <Box className={classes.mainHeadTypo}>
        <Typography className={classes.maskTypo}>{props.mainHead}</Typography>
      </Box>

      {props.shop ? (
        <Box className={classes.herosectionTypoContainer}>
          <Typography className={classes.typo}>{props.text}</Typography>
          <Button className={classes.button}>{props.buttonText}</Button>
        </Box>
      ) : (
        <></>
      )}
    </Box>
  );
};
export default HeroSection;