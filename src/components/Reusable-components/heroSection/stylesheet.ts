import { makeStyles } from "@material-ui/core";
import MaskedFontImage from "../../../../public/assets/images/main-heading.png";

export default makeStyles( theme => ({
  MainContainer: {
    display: "flex ",
    alignItems: "flex-start",
    paddingTop: "8rem",
    [theme.breakpoints.down('sm')]: {
      flexDirection:"column",
      justifyContent:"center",
      alignItems:"center"
    },
  
  },

  mainHeadTypo:{
    width:"50%",
    [theme.breakpoints.down('sm')]: {
      width:"100%",
      textAlign:"center"
    },
    [theme.breakpoints.down('xs')]: {
      textAlign:"justify"
    },
  },

  maskTypo: {
    fontFamily: "Orbitron",
    fontStyle: "normal",
    fontWeight: 900,
    fontSize: "150px",
    lineHeight: "150px",
    backgroundImage: `url(${MaskedFontImage.src})`,
    backgroundRepeat: "repeat",
    WebkitBackgroundClip: "text",
    WebkitTextFillColor: "transparent",
    WebkitFontSmoothing: "antialiased",
    [theme.breakpoints.down('md')]: {
      fontSize:"125px",
    },
    [theme.breakpoints.down('xs')]: {
      fontSize:"28vw",
      lineHeight: "140px",
      // lineHeight:"100px"
    },
  },
  herosectionTypoContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "0px",
    gap: "30px",
    width: "600px",
    height: "160px",
    marginLeft:"1rem",
    marginTop: "22px",
    [theme.breakpoints.down('md')]: {
      width:"100%",
      marginTop:"33px"
    },
    [theme.breakpoints.down('sm')]: {
      alignItems:"center",
      marginTop:"20px"
    },
    [theme.breakpoints.down('xs')]: {
      alignItems:"flex-start"
    },
  
  },
  typo: {
    fontFamily: "'Inter'",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "24px",
    lineHeight: "146.02%",
    flex: "none",
    order: 0,
    flexGrow: 0,
    [theme.breakpoints.down('md')]: {
      fontSize:"18px"
    },
    [theme.breakpoints.down('sm')]: {
      alignItems:"center",
      textAlign:"justify"
    },
    [theme.breakpoints.down('xs')]: {
      fontSize:"20px",
      textAlign:"start"
    },
  },
  button: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: "25px 75px",
    gap: "20px",
    height: "60px",
    border: "2px solid #FFFFFF",
    borderRadius: "10px",
    flex: "none",
    order: 1,
    flexGrow: 0,
    color: "#fff",
    [theme.breakpoints.down('xs')]: {
      padding:"25px 18px"
    },
  },

}));