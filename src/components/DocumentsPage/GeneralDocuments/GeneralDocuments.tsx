import React from 'react';
import useStyles from 'src/components/DocumentsPage/GeneralDocuments/GeneralDocuments.styles';
import { Box, Typography } from '@material-ui/core';
import Image from 'next/image';
import Link from 'next/link';

function GeneralDocuments({head, map}: any) {
    const classes = useStyles();
    return (
        <>
            {head === 'Hardware Datasheets' ? (
                <Box className={classes.mainContainer}>
                    <Box className={classes.general}>{head}</Box>
                     <Box className={classes.paper}>
                            <Box className={classes.modalContainer}>
                <span className={classes.mainHeading}>
                  Coming<br/>soon
                </span>
                            </Box>
                        </Box>
                    <Box className={classes.documentContainer} style={{filter: 'brightness(15%)',}}>
                        {map?.map(({id, title, subTitle, IconUrl, href}) => {
                            return (
                                <Link
                                    style={{
                                        pointerEvents: href == '/' ? 'none' : 'initial',
                                    }}
                                    key={id}
                                    href={href}
                                >
                                    <Box
                                        style={{
                                            pointerEvents: href == '/' ? 'none' : 'initial',
                                            cursor: href == '/' ? 'none' : 'pointer',
                                        }}
                                        key={id}
                                        className={classes.documentBox}
                                    >
                                        <Image
                                            width={25}
                                            height={32}
                                            src={IconUrl}
                                            alt="documents"
                                        />
                                        <Typography
                                            style={{
                                                fontWeight: 500,
                                                fontSize: '15px',
                                                marginTop: '10px',
                                            }}
                                        >
                                            {title}
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '10px',
                                                letterSpacing: '0.5px',
                                            }}
                                        >
                                            {subTitle}
                                        </Typography>
                                    </Box>
                                </Link>
                            );
                        })}

                    </Box>
                </Box>
            ) : (
                <Box className={classes.mainContainer}>
                    <Box className={classes.general}>{head}</Box>
                    <Box className={classes.documentContainer}>
                        {map?.map(({id, title, subTitle, IconUrl, href}) => {
                            return (
                                <Link
                                    style={{
                                        opacity: href == '/' ? 0.15 : 1,
                                        pointerEvents: href == '/' ? 'none' : 'initial',
                                    }}
                                    key={id}
                                    href={href}
                                >
                                    <Box
                                        style={{
                                            opacity: href == '/' ? 0.15 : 1,
                                            pointerEvents: href == '/' ? 'none' : 'initial',
                                            cursor: href == '/' ? 'none' : 'pointer',
                                        }}
                                        key={id}
                                        className={classes.documentBox}
                                    >
                                        <Image
                                            width={25}
                                            height={32}
                                            src={IconUrl}
                                            alt="documents"
                                        />
                                        <Typography
                                            style={{
                                                fontWeight: 500,
                                                fontSize: '15px',
                                                marginTop: '10px',
                                            }}
                                        >
                                            {title}
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: '10px',
                                                letterSpacing: '0.5px',
                                            }}
                                        >
                                            {subTitle}
                                        </Typography>
                                    </Box>
                                </Link>
                            );
                        })}
                    </Box>
                </Box>
            )}
        </>
    );
}

export default GeneralDocuments;
