import { makeStyles } from '@material-ui/core';
import MaskedFontImage from '@/public/assets/images/font.png';

export default makeStyles((theme) => ({
    mainContainer: {
        marginTop: '3rem',
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '3rem',
        position: 'relative',
        [theme.breakpoints.down('xs')]: {
            marginBottom: '1rem',
            marginTop: '1rem'
        },
        '@media(max-width: 430px)': {
            paddingBottom: '2rem',
        },
    },
    general: {
        fontSize: '47px',
        fontWeight: 500,
        lineHeight: '100px',
        [theme.breakpoints.down('sm')]: {
            fontSize: '23px',
        },
    },
    documentContainer: {
        display: 'grid',
        position: 'relative',
        gridTemplateColumns: '150px 150px 150px 150px 150px 150px',
        gridGap: '15px',
        [theme.breakpoints.down('sm')]: {
            gridTemplateColumns: '150px 150px 150px 150px',
            gridGap: '20px',
        },
        [theme.breakpoints.down('xs')]: {
            gridTemplateColumns: '150px 150px',
            gridGap: '15px',
        },
    },
    documentBox: {
        backgroundColor: '#16191E',
        borderRadius: '8px',
        padding: '20px',
        border: '2px solid #fff',
        '&:hover': {
            background: '#505C6B',
        }
    },
    paper: {
        position: 'absolute',
        zIndex: 3,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        left: '5%',
        top: '56%',
        [theme.breakpoints.down('sm')]: {
            left: '17%',
            top: '46%',
        },
    },
    modalContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        outline: 'none',
        '&:hover': {
            border: 'none',
        },
    },
    mainHeading: {
        fontFamily: '\'Orbitron\'',
        fontStyle: 'normal',
        fontWeight: 900,
        fontSize: '40px',
        backgroundImage: `url(${MaskedFontImage.src})`,
        WebkitBackgroundClip: 'text',
        WebkitTextFillColor: 'transparent',
        backgroundClip: 'text',
        textFillColor: 'transparent',
        textTransform: 'uppercase',
        textAlign: 'center',
        outline: 'none',
        lineHeight: '1',

    },
}));
