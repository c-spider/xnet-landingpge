import { Box, Typography } from '@material-ui/core'
import React from 'react'
import BackButton from '../Reusable-components/Back-Button/BackButton'
import useStyles from 'src/components/PrivacyContent/PrivacyContent.styles'

const PrivacyContent = () => {
    const classes = useStyles()

  return (
    <Box className={classes.mainContainer}>
        <BackButton />
        <Typography className={classes.mainHeading}>Privacy Policy</Typography>

        <Typography className={classes.innerContent}>{`XNET, Inc. (“XNET”, “us”, “our”, and “we”) is committed to maintaining robust privacy protections for its users.  Our Privacy Policy (“Privacy Policy”) is designed to help you understand how we collect, use and safeguard the information you provide to us and to assist you in making informed decisions when using our Service.`}</Typography>
        <Typography className={classes.boldInnerContent}>{`By accepting our Privacy Policy and Terms of Use (found here: located at xnet.company/terms), you consent to our collection, storage, use and disclosure of your personal information as described in this Privacy Policy. Capitalized terms in this policy have the same meaning given to them in our Terms of Use, unless otherwise defined in this policy.`}</Typography>
    
        <Typography className={classes.subHeading}>{`Children’s Online Privacy Protection Act`}</Typography>
    
        <Typography className={classes.innerContent}>{`Minors under eighteen (18) years of age are prohibited from using the Site and Services. The Site and Services are not intended for children under the age of 18. If you are under 18 years old, please do not provide personally identifiable information of any kind whatsoever on the Site or Services. We do not intentionally gather Personal Information from visitors who are under the age of 13. In the event that we learn that we have collected personal information from a child under age 13 without parental consent, we will delete that information as quickly as possible. If you believe that we might have any information from or about a child under 13, please contact us.`}</Typography>

        <Typography className={classes.subHeading}>{`Types of Data We Collect`}</Typography>

        <Typography className={classes.innerContent}>{`We collect “Non-Personal Information” and “Personal Information.” Non-Personal Information includes information that cannot be used to personally identify you, such as anonymous usage data, general demographic information we may collect, referring/exit pages and URLs, platform types, preferences you submit and preferences that are generated based on the data you submit and number of clicks. “Personal Information” means data that allows someone to identify or contact you, including, for example, your name, address, telephone number, e-mail address, as well as any other non-public information about you that is associated with or linked to any of the foregoing data.`}</Typography>

        <Typography className={classes.subHeading}>{`Information You Provide to Us`}</Typography>

        <ul className={classes.ulContainer}>
            <li>
                <Typography className={classes.innerContentUL}>{`We may collect Personal Information from you, such as your first and last name, e-mail and mailing address, phone number and password when you complete a contact form on our Site, or create or log-in to your Account.`}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`When you subscribe to the Services on our Site our payment processor will collect all information necessary to complete the transaction, including your name, credit card information, billing information and direct deposit information. `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`We retain information on your behalf, such as files and messages that you store using your Account. `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`If you provide us feedback or contact us via e-mail, we will collect your name and e-mail address, as well as any other content included in the e-mail, in order to send you a reply. `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`When you participate in one of our surveys, we may collect additional information that you knowingly provide. `}</Typography>
            </li>
        </ul>

        <Typography className={classes.subHeading}>{`Information Collected via Technology`}</Typography>

        <ul>
            <li>
                <Typography className={classes.innerContentUL}>{`In an effort to improve the quality of the Service, we track information provided to us by your browser or by our software application when you view or use the Service, such as the website you came from (known as the “referring URL”), the type of browser you use, the device from which you connected to the Service, the time and date of access, and other information that does not personally identify you.  We track this information using cookies, or small text files which include an anonymous unique identifier.  Cookies are sent to a user’s browser from our servers and are stored on the user’s computer hard drive. Sending a cookie to a user’s browser enables us to collect Non-Personal information about that user and keep a record of the user’s preferences when utilizing our services, both on an individual and aggregate basis. We may also ask advertisers or other partners to serve ads to your devices, which may use cookies or similar technology.  `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`We may also use third party analytics services such as Google Analytics or Google Adsense to collect information about how you use and interact with our Services. Such third party analytics services may use cookies to gather information such as the pages you visited, your IP address, a date/time stamp for your visit and which site referred you to the Site. This helps us provide a better user experience and improve usability.`}</Typography>
            </li>
        </ul>

        <Typography className={classes.subHeading}>{`Use of Your Personal Information`}</Typography>
        <Typography className={classes.innerContent}>{`In general, Personal Information you submit to us is used either to respond to requests that you make, or to aid us in serving you better. We use your Personal Information in the following ways:`}</Typography>
        <ul>
            <li>
                <Typography className={classes.innerContentUL}>{`to facilitate the creation of and secure your Account on our network; `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`identify you as a user in our system;  `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`provide improved administration of our Services; `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`provide the Services you request;  `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`improve the quality of experience when you interact with our Services; `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`send you a welcome e-mail to verify ownership of the e-mail address provided when your Account was created;  `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`send you administrative e-mail notifications, such as security or support and maintenance advisories; `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`to send newsletters, surveys, offers, and other promotional materials related to our Services and for other marketing purposes including those of third parties. Some of these activities may have additional rules, which could contain additional information about how we use and disclose your Personal Information, so it is important that you read the additional rules carefully; and  `}</Typography>
            </li>
            <li>
                <Typography className={classes.innerContentUL}>{`perform marketing or data analysis. `}</Typography>
            </li>
        </ul>

        <Typography className={classes.subHeading}>{`Use of Non-Personal Information`}</Typography>
        
        <Typography className={classes.innerContent}>{`In general, we use Non-Personal Information to help us improve the Service and customize the user experience. We also aggregate Non-Personal Information in order to track trends and analyze use patterns on the Site. This Privacy Policy does not limit in any way our use or disclosure of Non-Personal Information and we reserve the right to use and disclose such Non-Personal Information to our partners, advertisers and other third parties at our discretion.`}</Typography>
        <Typography className={classes.innerContent}>{`If our information practices change at any time in the future, we will post the policy changes to the Site so that you may opt out of the new information practices.  We suggest that you check the Site periodically if you are concerned about how your information is used. `}</Typography>

        <Typography className={classes.subHeading}>{`Location-Based Data`}</Typography>

        <Typography className={classes.innerContent}>{`XNET makes use of location-based data for our Site to deliver the Services. If you choose not to allow XNET to access your location, some or all functionality may not be available to you.`}</Typography>


        <Typography className={classes.subHeading}>{`Disclosure of Your Personal Information`}</Typography>
    
        <Typography className={classes.innerContent}>{`Our company does not sell your personal information.  We do, however, have relationships with third parties in order to provide our services to you in which your personal information may be shared.  We disclose your Personal Information as described below and as described elsewhere in this Privacy Policy.`}</Typography>
        <Typography className={classes.innerContent}>{`Third Party Service Providers. We may share your Personal Information with third party service providers to: provide you with the Services that we offer you through our Site; to conduct quality assurance testing; to perform marketing; to run data analysis; to facilitate creation of accounts; to provide technical support; and/or to provide other services to the Company. `}</Typography>
        <Typography className={classes.innerContent}>{`Other Disclosures. Regardless of any choices you make regarding your Personal Information (as described below), Company may disclose Personal Information if it believes in good faith that such disclosure is necessary (a) in connection with any legal investigation; (b) to comply with relevant laws or to respond to subpoenas or warrants served on Company; (c) to protect or defend the rights or property of Company or users of the Services; and/or (d) to investigate or assist in preventing any violation or potential violation of the law, this Privacy Policy, or Terms of Use.`}</Typography>

        <Typography className={classes.subHeading}>{`Links to Third Party Websites`}</Typography>
    
        <Typography className={classes.innerContent}>{`As part of the Service, we may provide links to or compatibility with other websites or applications.  However, we are not responsible for the privacy practices employed by those websites or the information or content they contain. This Privacy Policy applies solely to information collected by us through the Site and the Service. Therefore, this Privacy Policy does not apply to your use of a third party website accessed by selecting a link on our Site or via our Service. To the extent that you access or use the Service through or on another website or application, then the privacy policy of that other website or application will apply to your access or use of that site or application. We encourage our users to read the privacy statements of other websites before proceeding to use them.`}</Typography>

        <Typography className={classes.subHeading}>{`Your Rights Regarding the Use of Your Personal Information`}</Typography>

        <Typography className={classes.innerContent}>{`You have the right at any time to prevent us from contacting you for marketing purposes.  When we send a promotional communication to a user, the user can opt out of further promotional communications by following the unsubscribe instructions provided in each promotional e-mail.  You can also indicate that you do not wish to receive marketing communications from us in the “Settings” section of your Account.  Please note that notwithstanding the promotional preferences you indicate by either unsubscribing or opting out in the Settings section of your Account, we may continue to send you administrative emails including, for example, periodic updates to our Privacy Policy.`}</Typography>
    
        <Typography className={classes.subHeading}>{`Access, Correction, Deletion`}</Typography>

        <Typography className={classes.innerContent}>{`We respect your privacy rights and provide you with reasonable access to the Personal Information that you may have provided through your use of the Services. If you wish to access or amend any other Personal Information we hold about you, or to request that we delete any information about you that we have obtained from our Service, you may contact us at: info@xnet.company with the email subject as “Personal Information Query.” `}</Typography>
    
        <Typography className={classes.innerContent}>{`At your request, we will have any reference to you deleted or blocked in our database. `}</Typography>
        <Typography className={classes.innerContent}>{`You may update, correct, or delete your Account information and preferences at any time by accessing your Account settings page on the Service. Please note that while any changes you make will be reflected in active user databases instantly or within a reasonable period of time, we may retain all information you submit for backups, archiving, prevention of fraud and abuse, analytics, satisfaction of legal obligations, or where we otherwise reasonably believe that we have a legitimate reason to do so. You may decline to share certain Personal Information with us, in which case we may not be able to provide to you some of the features and functionality of the Service. `}</Typography>
        <Typography className={classes.innerContent}>{`At any time, you may object to the processing of your Personal Information, on legitimate grounds, except if otherwise permitted by applicable law.`}</Typography>
    
        <Typography className={classes.subHeading}>{`Security of Your Personal Information`}</Typography>

        <Typography className={classes.innerContent}>{`We implement security measures designed to protect your information from unauthorized access.  Your account is protected by your account password and we urge you to take steps to keep your personal information safe by not disclosing your password and by logging out of your account after each use.  We further protect your information from potential security breaches by implementing certain technological security measures However, these measures do not guarantee that your information will not be accessed, disclosed, altered or destroyed by breach of such firewalls and secure server software.  While we use reasonable efforts to protect your Personal Information, we cannot guarantee its absolute security. By using our Service, you acknowledge that you understand and agree to assume these risks.`}</Typography>


        <Typography className={classes.subHeading}>{`How Long We Retain Your Data`}</Typography>

        <Typography className={classes.innerContent}>{`We will keep hold of your data for no longer than necessary. The length of time we retain it will depend on any legal obligations we have (such as tax recording purposes), the nature of any contracts we have in place with you, the existence of your consent or our legitimate interests as a business.`}</Typography>
    
        <Typography className={classes.subHeading}>{`Do Not Track Signals`}</Typography>
        <Typography className={classes.innerContent}>{`Certain state laws require us to indicate whether we honor “Do Not Track” settings in your browser. XNET adheres to the standards set out in this Privacy Policy and does not monitor or follow any Do Not Track browser requests. `}</Typography>

        <Typography className={classes.subHeading}>{`Notices`}</Typography>

        <Typography className={classes.innerContent}>
            <a style={{ color: "#fff", textDecoration: "none" }}>Notice to California Residents.</a> {`The California Consumer Privacy Act (“CCPA”) provides California consumers with specific rights regarding their Personal Information. You have the right to request that businesses subject to the CCPA: (1) disclose certain information to you about their collection and use of your Personal Information over the past 12 months; (2) delete Personal Information collected from you, subject to certain exceptions; and (3) ask whether the business sells Personal Information and right to opt-out of that sale. To request the above information, you may contact us at: info@xnet.company with the email subject as California Disclosure Information.  `}
        </Typography>

        <Typography className={classes.innerContent}>
        California law requires that we provide you with a summary of your privacy rights under the California Online Privacy Protection Act (“California Act”) and the California Business and Professions Code. As required by the California Act, this Privacy Policy identifies the categories of personally identifiable information that we collect through our Site about individual consumers who use or visit our Site and the categories of third-party persons or entities with whom such personally identifiable information may be shared. 
        </Typography>

        <Typography className={classes.innerContent}>
            <a style={{ color: "#fff", textDecoration: "none" }}>Notices to International Users.</a> {`The Site is hosted in the United States. If you are a User accessing the the Site  or Services (or providing information that is hosted on or routed through the Site) from the European Union (“EU”), Asia, or any other region with laws or regulations governing personal data collection, use, and disclosure, that differ from United States laws, please note that you are transferring your personal data to the United States which does not have the same data protection laws as the EU and other regions, and by providing your personal data you permit the use of your personal data for the uses identified above in accordance with the Privacy Policy. `}
        </Typography>

        <Typography className={classes.subHeading}>{`Changes to This Privacy Policy`}</Typography>

        <Typography className={classes.innerContent}>{`We reserve the right to change this policy and our Terms of Service at any time.  We will notify you of significant changes to our Privacy Policy by sending a notice to the primary email address specified in your account or by placing a prominent notice on our site.  Significant changes will go into effect 30 days following such notification. Non-material changes or clarifications will take effect immediately. You should periodically check the Site and this privacy page for updates.`}</Typography>

        <Typography className={classes.subHeading}>{`Contact Information`}</Typography>

        <Typography className={classes.innerContent}>{`If you have any questions regarding this Privacy Policy or the practices of the Site please contact us by sending an email to info@xnet.company. `}</Typography>
        <Typography className={classes.innerContent}>{`Last Updated: This Privacy Policy was last updated on August 26, 2022. `}</Typography>

    </Box>
  )
}

export default PrivacyContent