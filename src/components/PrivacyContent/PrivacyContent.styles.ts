import { makeStyles } from "@material-ui/core";

export default makeStyles({
    mainContainer: {
        paddingLeft: "8vw",
        paddingRight: "8vw",
        paddingTop: "4rem",
        paddingBottom: "4rem",
        backgroundColor: "#0E0E11"
    },
    mainHeading: {
        fontFamily: "'Orbitron'",
        fontStyle: "normal",
        fontWeight: 700,
        fontSize: "60px",
        lineHeight: "75px",
        background:
          "linear-gradient(221.21deg, #75FFF6 23.34%, #37BBF8 42.85%, #0685F9 77.24%)",
        WebkitBackgroundClip: "text",
        WebkitTextFillColor: "transparent",
        backgroundClip: "text",
        textFillColor: "transparent",
        marginBottom: "4rem"
    },
    subHeading: {
        fontFamily: "'Roboto'",
        fontStyle: "normal",
        fontWeight: 700,
        fontSize: "32px",
        lineHeight: "38px",
        marginBottom: "1rem"
    },
    innerContent: {
        fontFamily: "'Roboto'",
        fontStyle: "normal",
        fontWeight: 400,
        fontSize: "20px",
        lineHeight: "146.02%",
        marginBottom: "3rem",
        color: "background: rgba(255, 255, 255, 1)"
    },
    ulContainer: {
        marginBottom: "3rem" 
    },
    innerContentUL: {
        fontFamily: "'Roboto'",
        fontStyle: "normal",
        fontWeight: 400,
        fontSize: "20px",
        lineHeight: "146.02%",
        marginBottom: "1rem",
        color: "background: rgba(255, 255, 255, 1)"
    },
    boldInnerContent: {
        fontFamily: "'Roboto'",
        fontStyle: "normal",
        fontWeight: 700,
        fontSize: "20px",
        lineHeight: "146.02%",
        marginTop: "2rem",
        marginBottom: "3rem"
    },
    
})