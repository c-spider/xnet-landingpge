import React from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "src/components/DifferenceSection/DifferenceSection.styles";
import Image from "next/image";
import { DiffernceStaticData } from "@/static/Difference.static";
import { IDifferenceSection } from "@/types";
import useMediaQuery from '@mui/material/useMediaQuery'
import BgEffect from "./BgEffect";

const DifferenceSection = () => {
  const matches = useMediaQuery('(max-width:600px)')
  const classes = useStyles();
  
  return (
    <Box className={classes.head}>
    <BgEffect/>
    <Box className={classes.differenceSectionContainer}>
      <Box className={classes.differenceSectionPrimaryContainer}>
        <Box>
          <Box className={classes.mainHeadingContainer}>
            <Typography
              className={classes.mainHeadingTypo}
            >{`XNET: Making a Difference in Mobile`}</Typography>
          </Box>

          <Box className={classes.innerContentContainer}>
            {DiffernceStaticData?.map(
              ({ heading, content, id, Icon }: IDifferenceSection) => (
                <Box
                  style={{ marginBottom: id === 3 || id === 4 ? "0" : "2rem" }}
                  key={id}
                  className={classes.cardContainer}
                >
                  <Box className={classes.typoContainer}>
                    <Image width={ matches ? 50 : 50} className={classes.icon} src={Icon} alt="icon" />
                    <Typography className={classes.mainHeading}>
                      {heading}
                    </Typography>
                  </Box>
                  <Box className={classes.contentContainer}>
                  <Typography className={classes.mainContent}>
                    {content}
                  </Typography>
                  </Box>
                </Box>
              )
            )}
          </Box>
        </Box>
      </Box>
    </Box>
    </Box>
  );
};

export default DifferenceSection;