import { makeStyles } from "@material-ui/core";
import MaskedFontImage from "../../../public/assets/images/bubbles.png";

export default makeStyles((theme) => ({
  head: {
    backgroundImage: `url(${MaskedFontImage.src})`,
    // width:"450px",
    backgroundSize: "cover",
    // height:"450px"
    height: "52vw",
    "@media screen and (max-width: 1300px)": {
      height:"65vw"
    },
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {
      backgroundImage: "none !important",
      display: "flex",
      flexDirection: "column",
      height: "fit-content",
      paddingBottom: "2vw",
    },
  },
  differenceSectionContainer: {
    marginBottom: "3rem"
  },
  coverImg: {
    width: "100%",
  },
  differenceSectionPrimaryContainer: {
    width: "-webkit-fill-available",
    height: "-webkit-fill-available",
    top: 0,
    margin: "8rem 8rem 2em 8rem",
    [theme.breakpoints.down("sm")]: {
      margin: "2rem 4rem 2em 4rem",
    },
    [theme.breakpoints.down("xs")]: {
      margin: "2rem 2rem 2em 2rem",
    },
  },
  mainHeadingContainer: {
    [theme.breakpoints.down("sm")]: {
      marginBottom: "20px",
      width: "80%",
    },
    [theme.breakpoints.down("xs")]: {},
  },
  mainHeadingTypo: {
    fontFamily: "'Orbitron'",
    fontStyle: "normal",
    fontWeight: 900,
    fontSize: "3.2vw",
    lineHeight: "79px",
    background:
      "linear-gradient(221.21deg, #75FFF6 23.34%, #37BBF8 42.85%, #0685F9 77.24%)",
    WebkitBackgroundClip: "text",
    WebkitTextFillColor: "transparent",
    backgroundClip: "text",
    textFillColor: "transparent",
    [theme.breakpoints.down("sm")]: {
      fontSize: "5vw",
      lineHeight: "unset",
    },
  
  },
  innerContentContainer: {
    width: "-webkit-fill-available",
    height: "-webkit-fill-available",
    // position: "absolute",
    marginTop: "4rem",
    display: "grid",
    gridTemplateColumns: "50% 50%",
    gridGap:"5vw",
    [theme.breakpoints.down("sm")]: {
      marginTop: "0rem",
    },
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "80%",
    },
  },
  cardContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "0px",
    width: "40vw",
    [theme.breakpoints.down("sm")]: {
      width: "40vw",
      height: "fitContent",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90vw",
      height: "150px",
      justifyContent:"space-around"
    },
  },
  typoContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "0px",
    gap: "20px",
  },
  mainHeading: {
    fontFamily: "'Roboto'",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "2vw",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.5vw",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "3vw",
    },
  },
  contentContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    padding: "0px 0px 0px 90px",
    gap: "10px",
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {
      padding: "0px 20px 0px 10px",
      width:"95%"
    },
  },
  icon: {
    [theme.breakpoints.down("sm")]: {
      width: "10px",
    },
    [theme.breakpoints.down("xs")]: {
      width:"5px"
    },
  },
  mainContent: {
    fontFamily: "'Roboto'",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1vw",
    lineHeight: "28px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1vw",
      lineHeight: "14px",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2.5vw",
      lineHeight: "16px",
    },
  },
}));
