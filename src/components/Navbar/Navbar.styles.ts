import { makeStyles } from '@material-ui/core';


export default makeStyles((theme) => ({
    navbarContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'black',
        height: '140px',
        background: 'rgba(2, 0, 2, 0.7)',
        borderBottom: '1px solid #FFFFFF',
        backdropFilter: 'blur(60px)',
        padding: '0px 130px 0px 100px',

        '@media screen and (max-width: 1400px)': {
            padding: '0px 70px 0px 55px',
        },

        [theme.breakpoints.down('md')]: {
            padding: '40px 4.4vw',
        },

        [theme.breakpoints.down('xs')]: {
            padding: '0px 20px 0px 40px',
            gap: '0px',
            height: '104px',
            position: 'fixed',
            zIndex: 1,
            justifyContent: 'space-between',
        },
        '@media screen and (max-width: 800px)': {
            justifyContent: 'space-between',
            padding: '40px 7.5vw',
        },
    },
    navbarLinksContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        padding: '0px',
        gap: '40px',
        [theme.breakpoints.down('xs')]: {
        },
        '@media screen and (max-width: 800px)': {
            display: 'none',
        },
        '@media screen and (min-width: 1350px)': {
            paddingLeft: '50px',
        },
        '@media screen and (max-width: 1000px)': {
            gap: '30px',
        },
        '@media screen and (max-width: 850px)': {
            justifyContent: 'space-between',
            gap: '20px',
        },
    },
    navbarLinkTypo: {
        fontFamily: 'Roboto',
        fontWeight: 500,
        fontSize: '16px',
        lineHeight: '23px',
        '@media screen and (max-width: 800px)': {
            marginRight: '10vw',

        },
        [theme.breakpoints.down('xs')]: {
            marginRight: '12vw',
        },
    },
    active: {
        position: 'absolute',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingTop: '15px',
        width: '100%',
        top: '91%',
        left: 0,
        gap: '30px',
        fontSize: '1.5rem',
        background: 'black',
        transition: '0.5s',
        paddingBottom: '5rem',
        marginTop: '10px',
    },
    hamburger: {
        display: 'none',
        padding: '1rem',
        fontSize: '1.2rem',
        '@media screen and (max-width: 800px)': {
            display: 'block',
            zIndex: 15,
        },
    },
    icon: {
        cursor: 'pointer',
        fontSize: "2rem"
    },
    navbarSVG: {
        gap: '40px',
        cursor: 'pointer',
        display: 'flex',
        flexDirection: 'row',
        '@media screen and (max-width: 1000px)': {
            gap: '30px',
        },
        '@media screen and (max-width: 850px)': {
            justifyContent: 'space-between',
            gap: '20px',
        },
        '@media screen and (max-width: 800px)': {
            marginRight: '10vw',
        },
        [theme.breakpoints.down('xs')]: {
            marginRight: '12vw',
        },
    },
}));
