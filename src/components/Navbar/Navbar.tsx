import React, { useState } from "react";
import { Box, Typography } from "@material-ui/core";
import useStyles from "./Navbar.styles";
import Image from "next/image";
import Logo from "@/public/assets/images/xnet_logo.png";
import { NavbarLinks, NavbarSocialLinks } from "@/static/Navbar.static";
import { INavbar } from "@/types";
import { FaBars, FaTimes } from "react-icons/fa";
import { useRouter } from "next/router";
import Link from "next/link";
import useMediaQuery from "@mui/material/useMediaQuery";
const Navbar = () => {
  const [nav, setNav] = useState(false);
  const matching = useMediaQuery("(max-width:550px)");
  const classes = useStyles();
  const router = useRouter();

  const handlePageChange = (link) => {
    router.push(link);
  };

  const handleNav = () => setNav(!nav);

  return (
    <Box className={classes.navbarContainer}>
      <Box style={{ cursor: "pointer" }}>
        <Link href="/">
          <Image
            width={matching ? 70 : 110}
            height={matching ? 70 : 110}
            src={Logo}
            alt="logo"
          />
        </Link>
      </Box>
      <Box
        className={
          nav
            ? classes.navbarLinksContainer && classes.active
            : classes.navbarLinksContainer
        }
      >
        {NavbarLinks?.map(({ label, link, cursor }: INavbar, index) => (
          <Typography
            style={{
              opacity: cursor == "not-allowed" ? 0.35 : 1,
              pointerEvents: cursor == "not-allowed" ? "none" : "initial",
              cursor: cursor == "not-allowed" ? "none" : "pointer",
            }}
            className={classes.navbarLinkTypo}
            key={index}
            onClick={() => handlePageChange(link)}
          >
            {label}
          </Typography>
        ))}
        <Box className={classes.navbarSVG}>
          {NavbarSocialLinks?.map(({ SVG, link }, index) => (
            <Link key={index} href={link}>
              <Image
                width="24px"
                height="24px"
                key={index}
                src={SVG}
                alt="social-links"
              />
            </Link>
          ))}
        </Box>
      </Box>
      <Box className={classes.hamburger} onClick={handleNav}>
        {!nav ? (
          <FaBars className={classes.icon} />
        ) : (
          <FaTimes className={classes.icon} />
        )}
      </Box>
    </Box>
  );
};

export default Navbar;
