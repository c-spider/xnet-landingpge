import React from 'react';
import { Box, Typography } from '@material-ui/core';
import useStyles from './HeroSection.styles';
import Button from '@material-ui/core/Button';
import Link from 'next/link';
import useMediaQuery from '@mui/material/useMediaQuery';
import TextEffect from './TextEffect';

const HeroSection = () => {

    const matching = useMediaQuery('(max-width:550px)');
    const classes = useStyles();

    return (
        <Box className={classes.MainContainer}>
            <Box className={classes.headTypo}>
                {matching ? (
                    <Box
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        {/* <Typography className={classes.respTypo}>XNET1</Typography> */}
                        <div id='textEffect'>
                            <TextEffect/>
                        </div>
                    </Box>
                ) : (
                    // <Typography className={classes.maskTypo}>XNET1</Typography>
                    <div id='textEffect'>
                        <TextEffect/>
                    </div>
                )}
            </Box>
            <Box className={classes.herosectionTypoContainer}>
                <Typography className={classes.typo}>
                    The first blockchain-powered mobile carrier.
                </Typography>
                <Link href="/files/Whitepaper.pdf">
                    <Button
                        style={{color: '#fff', textTransform: 'none'}}
                        className={classes.button}
                    >
                        Whitepaper
                    </Button>
                </Link>
            </Box>
        </Box>
    );
};
export default HeroSection;
