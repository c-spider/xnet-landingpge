import { makeStyles } from "@material-ui/core";

export default makeStyles({
  mainContainer: {
    width: "auto",
    paddingLeft: "8rem",
    paddngRight: "8rem",
    marginTop: "10rem",
    marginBottom: "4rem"
  },
  mainHeadingTypo: {
    fontFamily: "'Orbitron'",
    fontStyle: "normal",
    fontWeight: 900,
    fontSize: "60px",
    lineHeight: "75px",
    background: "linear-gradient(264.05deg, #75FFF6 6.66%, #0685F9 44.39%)",
    WebkitBackgroundClip: "text",
    WebkitTextFillColor: "transparent",
    backgroundClip: "text",
    textFillColor: "transparent",
  },
  mapMainContainer: {
    position: "relative",
    marginTop: "10rem",
  },
  coverImg: {
    width: "100%",
    opacity: 0.9
  },
  typoContainer: {
    width: "-webkit-fill-available",
    height: "-webkit-fill-available",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0
  },
  mainTypo: {
    fontFamily: "'Orbitron'",
    fontStyle: "normal",
    fontWeight: 900,
    fontSize: "80px",
    lineHeight: "100px",
    color: "#FFFFFF",
    opacity: 0.6
  }
});
