import React from 'react'
import { Box, Typography } from "@material-ui/core"
import Image from 'next/image'
import Map from "@/public/assets/svgs/map.svg"
import useStyles from "./stylesheet"

const Network = () => {
    const classes = useStyles()
  return (
    <Box className={classes.mainContainer}>
        <Box>
            <Typography className={classes.mainHeadingTypo}>{`Curent Network`}</Typography>
        </Box>

        <Box className={classes.mapMainContainer}>
            <Image className={classes.coverImg} src={Map} priority={true} alt="global-map" />

            <Box className={classes.typoContainer}>
                <Typography className={classes.mainTypo}>{`COMING SOON...`}</Typography>
            </Box>
        </Box>
    </Box>
  )
}

export default Network