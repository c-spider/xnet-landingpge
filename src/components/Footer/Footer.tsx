import React from "react";
import useStyles from "src/components/Footer/Footer.styles";
import { Box, Typography } from "@material-ui/core";
import Link from "next/link";
import Image from "next/image";
import { footerMenu, footerSvg } from "@/static/footer.static";

function Footer() {
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <Box className={classes.footerMain}>
        <Box className={classes.Footer}>
          <Box
          className={classes.leftContainer}
          >
            <Typography className={classes.typo}>
              &copy; 2022 XNET Inc. All rights reserved.
            </Typography>
            {footerMenu.map(({ id, title, href }) => {
              return (
                <Link key={id} href={href}>
                  <a style={{color:"#0685F9" , cursor:"pointer"}} className={classes.footerLink}>{title}</a>
                </Link>
              );
            })}
          </Box>
          <Box
          className={classes.rightContainer}
          >
            {footerSvg.map(({ id, SVG, src }) => {
              return (
                <a key={id} href={src} target="_blank" rel="noreferrer">
                  <Image
                    style={{ cursor: "pointer" }}
                    key={id}
                    width="24px"
                    height="24px"
                    src={SVG}
                    alt="footer-social-icon"
                  />
                </a>
              );
            })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default Footer;