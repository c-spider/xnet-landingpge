import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  container: {
    width: "100%",
    borderTop: "1px solid #FFFFFF",
    paddingTop: "2rem",
    paddingBottom: "2rem",
    position: "relative",
    bottom: "0px",
    background: "#16181D"
  },
  footerMain:{
    width: "85%", 
    margin: "auto",
    "@media screen and (max-width: 1100px)": {
      width:"80%"
    },
    [theme.breakpoints.down("xs")]: {
      width:"85%"
    },

  },
  Footer: {
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
      justifyContent: "space-between",
      alignItems: "center",
    },
  },
  footerLink: {
    fontSize: "12px",
    [theme.breakpoints.down("xs")]: {
      fontSize:"8px"
    },
  },
  leftContainer: {
    display: "flex",
    width: "40%",
    "@media screen and (max-width: 1100px)": {
      width:"60%"
    }, 
    justifyContent: "space-evenly",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      justifyContent: "space-evenly",
      alignItems: "center",
      marginTop: "15px",
    },
  },
  rightContainer: {
    display: "flex",
    justifyContent: "space-between",
    width: "10%",
    "@media screen and (max-width: 1100px)": {
      width:"14%"
    }, 
    [theme.breakpoints.down("sm")]: {
      width: "30%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "40%",
    },
  },
  typo: {
    fontSize: "12px",
    letterSpacing: "0.5px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "10px",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize:"8px"
    },
  },
}));
