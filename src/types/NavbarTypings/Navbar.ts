export interface INavbar {
    label: string;
    link?: string;
    color?: string;
    cursor?: string;
}