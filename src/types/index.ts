import { IDifferenceSection } from "./DifferenceSectionTypings/DifferenceSection";
import { INavbar } from "./NavbarTypings/Navbar";

export type { IDifferenceSection, INavbar };