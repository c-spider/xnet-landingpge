export interface IDifferenceSection {
    heading: string;
    content: string;
    id: number;
    Icon: any;
}