import PrivacyContent from "@/components/PrivacyContent/PrivacyContent";
import Layout from "@/Layout/Layout";
import Head from "next/head";
import React from "react";

const PrivacyPolicy = () => {
  return (
    <Layout>
      <Head>
        <title>{`Privacy Policy`}</title>
      </Head>
      <PrivacyContent />
    </Layout>
  );
};

export default PrivacyPolicy;
