import React from "react";
import Layout from "../../src/Layout/Layout";
import {
  Box, createStyles, makeStyles, Theme,
  Typography,
} from '@material-ui/core';
import ShopHeroSection from "../../src/components/Reusable-components/heroSection/HeroSection";
import Products from "../../src/components/ShopPage/Products/Products";
import Head from "next/head";
import Accessories from "@/components/ShopPage/Accessories/Accessories";
import useMediaQuery from "@mui/material/useMediaQuery";
import MaskedFontImage from '@/public/assets/images/font.png'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            position: 'absolute',
            zIndex: 0,
            top: '17%',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            [theme.breakpoints.down('md')]: {
                top: '12%',
            },
            [theme.breakpoints.down('sm')]: {
                top: '17%',
            },
        },
        modalContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            outline: 'none',
            '&:hover': {
                border: 'none',
            },
        },
        mainHeading: {
            fontFamily: '\'Orbitron\'',
            fontStyle: 'normal',
            fontWeight: 900,
            fontSize: `17vw`,
            lineHeight: '19vw',
            backgroundImage: `url(${MaskedFontImage.src})`,
            WebkitBackgroundClip: 'text',
            WebkitTextFillColor: 'transparent',
            backgroundClip: 'text',
            textFillColor: 'transparent',
            textAlign: 'center',
            textTransform: 'uppercase',
            outline: 'none',
        },
        mainHeading1: {
            fontFamily: '\'Orbitron\'',
            fontStyle: 'normal',
            fontWeight: 900,
            fontSize: '23vw',
            textAlign: 'center',
            lineHeight: '101.4%',
            letterSpacing: '-0.05em',
            backgroundImage: `url(${MaskedFontImage.src})`,
            // background:
            //   "linear-gradient(221.21deg, #75FFF6 23.34%, #37BBF8 42.85%, #0685F9 77.24%)",
            WebkitBackgroundClip: 'text',
            WebkitTextFillColor: 'transparent',
            backgroundClip: 'text',
            textFillColor: 'transparent',
            outline: 'none',
        },
    }));
function Shop() {
  const classes = useStyles();
  const matching = useMediaQuery("(max-width:550px)");

  return (
    <Box style={{ overflow: "hidden", position: "fixed", width: "100%" }}>
      <Layout>
        <Head>
          <title>{`Shop`}</title>
        </Head>
        <Box style={{ marginBottom: "4rem" }}>
          <Box style={{ width: "100%", filter: "brightness(15%)" }}>
            <Box style={{ width: "85%", margin: "auto" }}>
              <ShopHeroSection
                text={
                  "XNET is going to be available in specific locations. Please enter your address to check if your location is within the boundaries of our initial cluster footprint."
                }
                buttonText={"Check Availability"}
                mainHead={"XNET Shop"}
                shop
              />
              <Products />
              <Accessories />
            </Box>
          </Box>
          <Box className={classes.paper}>
            <Box className={classes.modalContainer}>
              {matching ? (
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    // justifyContent:"center",
                    alignItems: "center",
                  }}
                >
                  <Typography
                    className={classes.mainHeading1}
                  >Coming<br/>Soon</Typography>
                </Box>
              ) : (
                <Typography
                  className={classes.mainHeading}
                >
                  Coming<br/>Soon
                </Typography>
              )}
            </Box>
          </Box>
        </Box>
      </Layout>
    </Box>
  );
}

export default Shop;
