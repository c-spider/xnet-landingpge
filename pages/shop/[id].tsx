import React from "react";
import Layout from "../../src/Layout/Layout";
import { useRouter } from "next/router";
import { products } from "@/static/shop.static";
import { Box } from "@material-ui/core";

function Id() {
  const router = useRouter();
  const { id } = router?.query;
  console.log(typeof id);
  const product = products.find((products: any) => {
    return products?.id === id;
  });
  return (
    <Layout>
      {product == undefined ? (
        <Box>Product not Found</Box>
      ) : (
        <Box>{product?.title}</Box>
      )}
    </Layout>
  );
}

export default Id;
