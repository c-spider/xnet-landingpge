import React from "react";
import Layout from "../src/Layout/Layout";
import { Box } from "@material-ui/core";
import ShopHeroSection from "@/components/Reusable-components/heroSection/HeroSection";
import GeneralDocuments from "@/components/DocumentsPage/GeneralDocuments/GeneralDocuments";
import { generalDocuments, hardwareDataSheets } from "@/static/Document.static";
import Head from "next/head";

function documents() {
  return (
    <Layout>
      <Head>
        <title>{`Documents`}</title>
      </Head>
      <Box>
        <Box style={{ width: "100%" }}>
          <Box style={{ width: "85%", margin: "auto" }}>
            <ShopHeroSection mainHead={"XNET Docs"} />
            <GeneralDocuments head={"General"} map={generalDocuments} />
            <GeneralDocuments
              head={"Hardware Datasheets"}
              map={hardwareDataSheets}
            />
          </Box>
        </Box>
      </Box>
    </Layout>
  );
}

export default documents;
