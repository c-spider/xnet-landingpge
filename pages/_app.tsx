import React, { useEffect } from 'react';
import 'pure-react-carousel/dist/react-carousel.es.css';
import "../styles/globals.css";

function MyApp(props: { Component: any; pageProps: any; }) {
  const { Component, pageProps } = props;

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
      <Component {...pageProps} />
  );
}

export default MyApp;
