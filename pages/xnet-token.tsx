import Layout from '@/Layout/Layout'
import Head from 'next/head'
import { Box } from '@material-ui/core'
import React from 'react'
import XnetTokensContent from '@/components/XnetTokens/XnetTokensContent'

function XnetToken() {
  return (
    <Layout>
      <Head>
        <title>{`$XNET Token`}</title>
      </Head>
      <Box>
        <Box style={{ width: "100%" }}>
            <XnetTokensContent/>
        </Box>
      </Box>
    </Layout>
  );
}

export default XnetToken;


