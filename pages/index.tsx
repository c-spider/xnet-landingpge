import React from 'react';
import { Box } from '@material-ui/core';
import HeroSection from '../src/components/HeroSection/HeroSection';
import Head from 'next/head';
import { Banner } from '@/components/Reusable-components';
import DifferenceSection from '@/components/DifferenceSection/DifferenceSection';
import News from '@/components/News/News';
import Layout from '../src/Layout/Layout'
import useStyles from '../src/styles';


export default function Home() {

    const classes = useStyles();

    return (
        <Layout>
            <Head>
                <title>{`Home`}</title>
            </Head>
            <Box style={{width: '100%'}}>
                <Box>
                    <HeroSection/>
                </Box>
                <Banner
                    mainText={`XNET is dedicated to providing wireless connectivity and economic opportunity to everyone through the world's first blockchain-powered true mobile carrier.`}
                    buttonText={'Learn More'}
                    link={'/documents'}
                    fontSize={'24px'}
                />
                <DifferenceSection/>
                <Box className={classes.BoxContainer}>
                    <Banner
                        mainText={`A next generation mobile network built on the intersection of legacy telecom and blockchain technology.`}
                        buttonText={'Get Started'}
                        fontSize={'24px'}
                        link={'/shop'}
                        mainTextMaxWidth={735}
                        contentContainer={'auto'}
                    />
                </Box>
                <News/>
            </Box>
        </Layout>
    );
}
